// Cube
const getCub = 2 ** 3
console.log(`The cube of 2 is ${getCub} `);

// Address
let address = [258, "Washington Ave NW", "California", 90011];
const [streetNum, streetName, state, zipCode] = address
console.log(`I live at ${streetNum} ${streetName}, ${state} ${zipCode}`);


let animal = {
		name: "Lolong",
		kind: "saltwater crocodile",
		weight: "1075 kgs",
		length: "20 ft 3 in"
}

const {name, kind, weight, length} = animal;
console.log(`${name} was a ${kind}. He weighed at ${weight} with a measurement of ${length}.`)

const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
	console.log(number);

})

const reduceNumber = numbers.reduce((acc, num) => acc + num, 0)
console.log(reduceNumber);


class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed
	}

}

const myDog = new Dog
myDog.name = "Molly"
myDog.age = 2
myDog.breed = "Belgian Malinois"
console.log(myDog);